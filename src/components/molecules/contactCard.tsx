import { useState } from 'react';
import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import EdiText from 'react-editext';

import { Card } from '@components/atoms';
import { IPerson } from '@lib/models/person';

export interface IContactCardProps {
  person: IPerson;
  onEdit(id: string): void;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { id, firstName, lastName, email },
  onEdit,
  sx
}) => {

  const [editedFirstName, setEditedFirstName] = useState(firstName);
  const [editedLastName, setEditedLastName] = useState(lastName);

  const handleSaveFirstName = (val: any) => {
    console.log('Edited Value -> ', val);
    setEditedFirstName(val);
  };
  
  const handleSaveLastName = (val: any) => {
    console.log('Edited Value -> ', val);
    setEditedLastName(val);
  };

  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar sx={{ mr: 1.5 }} />
        <Box textAlign="center" mt={2}>
          <Typography variant="subtitle1" lineHeight="1rem">
            {<EdiText type="text" value={firstName} onSave={handleSaveFirstName} />} {<EdiText type="text" value={lastName} onSave={handleSaveLastName} />}
          </Typography>
          <Typography variant="caption" color="text.secondary">
            {email}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};
